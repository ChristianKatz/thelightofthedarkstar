﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the script is attached on the container for the letters
public class solvePuzzle1 : MonoBehaviour
{
    // shows if the correct letter is in the container
    public bool LetterUisActivated;

    // that represents the letter color
    [SerializeField]
    private GameObject activateParticlesystem;

    void Start()
    {
        // at the beginning is nothing in the container
        LetterUisActivated = false;

        // the particle system is deactivated at the beginning
        activateParticlesystem.SetActive(false);
    }

    // if the letter U stays on the correct place, the bool value will set to true
    // the particle system will start 
    public void OnTriggerStay(Collider other)
    {      
        if(other.gameObject.name == "LetterU")
        {
            LetterUisActivated = true;
            activateParticlesystem.SetActive(true);
        }                       
    }

    // if the letter U isn't in the container, the bool value will set to false
    // the particle system will be deactivated if the letter is not in the correct container
    public void OnTriggerExit(Collider other)
    {     
        if (other.gameObject.name == "LetterU")
        {
            LetterUisActivated = false;
            activateParticlesystem.SetActive(false);
        }       
    }
}
