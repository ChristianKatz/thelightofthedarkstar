﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// when the player picks up the medallion the planet shakes and the home will be devasted 
// for that the scene will change and I have to pick many items to do that
public class getMedallion : MonoBehaviour
{
    // Text to show the f buttton
    [SerializeField]
    private TextMeshProUGUI PressF;

    // the script for setting the movement of the player 0
    private PlayerMovement playerMovement;

    // the texture for the cursor to change it when the player points on the medallion
    [SerializeField]
    private Texture2D CursorTexture;

    // the animator for the shaking camera if the player clicks on the medallion
    [SerializeField]
    private Animator shakingCamera;

    // the animator to have a smooth transition in the next scene after the camera shaking
    [SerializeField]
    private Animator Fading;

    // take the mesh renderer to make the medallion invisible 
    [SerializeField]
    private MeshRenderer Medallion;

    // voice should be played when the medallion is collected
    [SerializeField]
    private GameObject Audio;

    // the door opens when the medaillon is picked up
    [SerializeField]
    private GameObject Door;

    // collider of the medaillon is deactivated after it has been collected
    private Collider collider;

    // fog is activated after the medaillon has been picked up
    [SerializeField]
    private GameObject Fog;

    // the debris will begin to fly around after the medallion has been collected
    [SerializeField]
    private GameObject FlyingDebris;

    // all signals will begin to start after the medaillion has been picked up
    [SerializeField]
    private GameObject[] LetterSignal;

    // new items location after the earthquake
    [SerializeField]
    private GameObject AfterEarthquake;
    [SerializeField]

    // items location before the earthquake
    private GameObject BeforeEarthquake;

    // the flickering light is activated after the earthquake
    public bool activateLight;
       
    void Start()
    {
        // get the script
        playerMovement = FindObjectOfType<PlayerMovement>();

        // button f should only be activated if the player stepps in the trigger
        PressF.gameObject.SetActive(false);  

        // the voice shouldn't be played yet
        Audio.SetActive(false);

        // get the collider
        collider = GetComponent<Collider>();

        // fog is deactivated at the beginning
        Fog.SetActive(false);

        // flying debris are deactivated at the beginning
        FlyingDebris.SetActive(false);

        // two letter signals are deactivated
        LetterSignal[0].SetActive(false);
        LetterSignal[1].SetActive(false);

        // the flickering light is deactivated
        activateLight = false;
        AfterEarthquake.SetActive(false);      
        
    }

    // if the player steps in the trigger, he sees that he can press the f button
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            PressF.gameObject.SetActive(true);
        }
    }

    // if the player stays in the trigger, he can collect the medallion
    // the player can't move anymore
    // planet starts to shake in the coroutine that will be activated
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            if (Input.GetKey(KeyCode.F))
            {
                Medallion.enabled = false;
                PressF.gameObject.SetActive(false);              
                StartCoroutine(StopShaking());
                playerMovement.speed = 0;
                
            }
        }
    }

    //if the player leaves the trigger, he isn't able to get the medallion anymore
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            PressF.gameObject.SetActive(false);
        }
    }

    // if the player points on the medallion, the cursor will change
    private void OnMouseEnter()
    {
        Cursor.SetCursor(CursorTexture, new Vector2(0, 0), CursorMode.Auto);
    }

    // if the cursor leaves the medallion, the cursor will change
    private void OnMouseExit()
    {
        Cursor.SetCursor(playerMovement.CursorTexture, new Vector2(0, 0), CursorMode.Auto);
    }

    // camera is shaking for 2 seconds
    // character will be unconscious
    // he wakes up again and the shaking stops
    // character voice, fog, debris, letter signals, flickering light and the new placement of the items are activated
    // now is the planet devasted for the player
    IEnumerator StopShaking()
    {
        shakingCamera.SetBool("CameraIsShaking", true);
        yield return new WaitForSeconds(2);
        Fading.SetTrigger("FadeOut");
        yield return new WaitForSeconds(2);
        shakingCamera.SetBool("CameraIsShaking", false);
        Fading.SetTrigger("FadeIn");
        
        Audio.SetActive(true);
        Door.SetActive(false);
        collider.enabled = false;
        Fog.SetActive(true);
        FlyingDebris.SetActive(true);
        LetterSignal[0].SetActive(true);
        LetterSignal[1].SetActive(true);
        activateLight = true;
        AfterEarthquake.SetActive(true);
        BeforeEarthquake.SetActive(false);
        playerMovement.speed = 10;       
    }
}
