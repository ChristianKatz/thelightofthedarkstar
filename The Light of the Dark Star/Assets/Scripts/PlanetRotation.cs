﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// rotates the sun and the moon around the map 
public class PlanetRotation : MonoBehaviour
{
    // the rotation speed of the planet
    public float Rotationspeed;

    void Update()
    {
        // it rotates the planet around the y-axis
        transform.Rotate(new Vector3(0, Rotationspeed * Time.deltaTime, 0));
    }
}
