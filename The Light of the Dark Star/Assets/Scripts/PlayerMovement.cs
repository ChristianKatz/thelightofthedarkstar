﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the player controlls
public class PlayerMovement : MonoBehaviour
{
    // the run speed of the character
    [SerializeField]
    public int speed;

    // the speed is for the character rotation 
    [SerializeField]
    private int rotateSpeed;

    // contains the input value of the mouse
    private float PlayerRotation;

    // the new texture for the cursor
    [SerializeField]
    public Texture2D CursorTexture;   

    void Start()
    {
        // the texture for the cursor
        Cursor.SetCursor(CursorTexture, new Vector2(0, 0), CursorMode.Auto);
    }

    void Update()
    {
        // get the inputs for the movement of the character
        PlayerRotation = Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
        float moveHorizontal = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float moveVertical = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        // the character gets the values to move
        transform.Translate(moveHorizontal, 0, moveVertical);
        transform.Rotate(0, PlayerRotation, 0);       
    }
}
   

