﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the script is for the letter puzzle in the second level
// it respawns the letter if the player wants that
public class LetterRespawn : MonoBehaviour
{
    // the GamObject to displays the text of the F Button
    [SerializeField]
    private GameObject PressFForReset;

    // the different letters
    [SerializeField]
    private GameObject LetterL;
    [SerializeField]
    private GameObject LetterU;
    [SerializeField]
    private GameObject LetterK;
    [SerializeField]
    private GameObject SecondLetterU;
    [SerializeField]
    private GameObject LetterA;

    // the respawn position
    [SerializeField]
    private GameObject Respawn;
 
    void Start()
    {
        // the press f text should only be visible if the player hits the trigger box where he can reset it
        PressFForReset.SetActive(false);
    }

    // if the trigger is hit the reset button will be visible for the player
    private void OnTriggerEnter(Collider other)
    {
        PressFForReset.SetActive(true);
    }

    // if the player stays in the trigger box he is able to press the F button to respawn the letters
    private void OnTriggerStay(Collider other)
    {
        if(Input.GetKey(KeyCode.F))
        {
            LetterL.transform.position = Respawn.transform.position;
            LetterU.transform.position = Respawn.transform.position;
            LetterK.transform.position = Respawn.transform.position;
            SecondLetterU.transform.position = Respawn.transform.position;
            LetterA.transform.position = Respawn.transform.position;
        }
    }
    // when the player leaves the trigger box the f key text disappears
    private void OnTriggerExit(Collider other)
    {
        PressFForReset.SetActive(false);
    }
}
