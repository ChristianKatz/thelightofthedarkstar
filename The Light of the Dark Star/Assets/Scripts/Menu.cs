﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// that is the main menu
public class Menu : MonoBehaviour
{
    // method for a button, that starts the game
    public void Play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // method for a button, that quits the game
    public void Quit()
    {
        Application.Quit();
    }    
}
