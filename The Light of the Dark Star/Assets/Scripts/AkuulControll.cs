﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//that is a script to move the character akuul, who is for a short time visible in the first level
//I can decide in which direction and speed he should move and I can decide when he should disappear or appear
public class AkuulControll : MonoBehaviour
{  
    // GameObject that has the character Akuul
    public GameObject Akuul;

    // bool value that shows "true" if akuul appears
    private bool AkuulisThere;

    // 3 different int values for the axis to set the speed of him
    public int Xspeed;
    public int Yspeed;
    public int Zspeed;

    // the time in which akuul should disappear
    public int AkuulDestroyTime;

    // the voice that he should get for the litte act
    public AudioSource AkuulVoice;    

    // the time in which akuul waits to appear if the trigger box has been touched
    public int AkuulWaitingToMove;

    // contains the animation for akuul
    public Animator animator;

    // bool value that shows "true" if acuul is disappeared 
    private bool AkuulIsDeactivated;

    private void Start()
    {
        //akuul is deactivated, because he will be activated by a trigger 
        Akuul.SetActive(false);

        // the two bool values aren't true because nothing should happen if the player is not around
        AkuulisThere = false;
        AkuulIsDeactivated = false;

        // akuul shouldn't talk if he is not there
        AkuulVoice.gameObject.SetActive(false);        
    }
    private void Update()
    {       
        // if akuul appears, the coroutine starts to move akuul
        if (AkuulisThere == true)
        {            
            StartCoroutine(AcculMoving());
        }

        // if akuul shoud be deactivated, the coroutine starts to disapear akuul
        if (AkuulIsDeactivated == true)
        {
            StartCoroutine(AcculDisappear());
        }
    }

    // Akuul will be visible if the player walks into the trigger
    // different methods will be activated to control akuul's behaviour
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            Akuul.SetActive(true);
            AkuulisThere = true;
            AkuulIsDeactivated = true;
            AkuulVoice.gameObject.SetActive(true);                                            
        }
    }
    // akuul will appear and walk
    // animation will be played
    IEnumerator AcculMoving()
    {
        yield return new WaitForSeconds(AkuulWaitingToMove);
        Akuul.transform.Translate(new Vector3(Xspeed * Time.deltaTime, Yspeed * Time.deltaTime, Zspeed * Time.deltaTime));
        animator.SetBool("moving", true);       
    }
    // akuul will be dissapear
    // akuul's animation will be canceled
    IEnumerator AcculDisappear()
    {
        yield return new WaitForSeconds(AkuulDestroyTime);
        animator.SetBool("moving", false);
        Akuul.SetActive(false);
    }

}
