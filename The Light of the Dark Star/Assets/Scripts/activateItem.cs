﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this is the script which is on one object of the 5 in the second level
public class activateItem : MonoBehaviour
{
    // this GameObject displays the F button on the screen
    public GameObject pressF;

    // this AudioSource is for the little story for each item
    public AudioSource Voice;

    // this bool value shows if the letter is activated or not
    public bool LetterIsActivated;

    // this Gamobject contains a letter, that will spawn if the player interactes with the item
    public GameObject Letter;
  
    // float value, that counts the distance between the player and the stones
    private float Distance;

    // Transform for the player to get the current position
    public Transform Player;   

    // Gameobject, where the stones will move up
    public GameObject MoveUp;

    // Gameobject, where the stones will move down
    public GameObject MoveDown;

    // Gamobject, that contains the stones, which will move up and down
    public GameObject checkDistance;
     
    void Start()
    {
        // button f is deactivated because the player shouldn't see the button immediately
        pressF.SetActive(false);

        // voice is deactivated because the player should only listen to the story if he interacts with the item
        Voice.gameObject.SetActive(false);        

        // letter is deactivated because it should only spawn if the player interacts with the item
        Letter.SetActive(false);

        // shows if the letter is activated or not
        LetterIsActivated = false;
    }
   
    void Update()
    {
        // calculating the current distance between the stones and the player to decide if the stones should move up or down
        Distance = transform.position.z - Player.position.z;
        if (Distance < 10)
        {
            checkDistance.transform.position = Vector3.MoveTowards(checkDistance.transform.position, MoveUp.transform.position, Distance * Time.deltaTime / 4);           
        }
        if (Distance > 10)
        {           
            checkDistance.transform.position = Vector3.MoveTowards(checkDistance.transform.position, MoveDown.transform.position, Distance * Time.deltaTime / 6);
        }
    }

    // if the player touches the trigger, he will see that he can interact with the item
    // the letter will spawn at the door
    private void OnTriggerEnter(Collider other)
    {
        pressF.SetActive(true);
    }

    // if the player stays in the trigger and presses the f button, he can listen to the voice
    // the letter will spawn at the door
    private void OnTriggerStay(Collider other)
    {                  
            if (Input.GetKey(KeyCode.F))
            {               
                Voice.gameObject.SetActive(true);
                pressF.SetActive(false);
                Letter.SetActive(true);
                LetterIsActivated = true;
            }                                                         
    }

    // if the player exits the trigger, the voice will stop
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.name =="Player")
        {
            Voice.gameObject.SetActive(false);
            pressF.SetActive(false);           
        }
        
    }
}
