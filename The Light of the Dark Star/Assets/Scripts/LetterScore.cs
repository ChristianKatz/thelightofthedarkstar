﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the script is for the counting of the letters in the first level
// actually this script to count is not necessary, because the letters disappear anyway when the player picks them
// but in the first version of the game the player was able to read the letters on the map every time and I had to make sure that the counter doesn't count higher, even though the player has already found the letter
// so I decided to do it like this and didn't change it after the letter inventory
// also the script is attached to the object, that will be deactivated
public class LetterScore : MonoBehaviour
{
    // that is the GameObject that contains any object
    [SerializeField]
    private GameObject Counter;

    // need the script to count the letters
    private LetterCounter letterScore;

    // need the script to give the signal that the letter is collected
    [SerializeField]
    private ItemInteraction ItemInteraction;
 
    void Start()
    {       
        // get the script
        letterScore = FindObjectOfType<LetterCounter>();       
    }

    void Update()
    {
        // if a letter has been collected, the counter will be set to + 1
        // the object will be deactivated so that the counter can't get any higher
        if(ItemInteraction.ActivateLetter == true)
        {
            letterScore.Counting += 1;
            Counter.SetActive(false);         
        }
    }
}
