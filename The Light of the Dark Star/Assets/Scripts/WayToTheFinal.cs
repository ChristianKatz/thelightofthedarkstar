﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the script, that opens the door for the third level
public class WayToTheFinal : MonoBehaviour
{  
    // need these scripts to check if all letters are in the correct container
    solvePuzzle LetterL;
    solvePuzzle1 LetterU;
    solvePuzzle2 LetterK;
    solvePuzzle3 LetterU2;
    solvePuzzle4 LetterA;

    // voice line of akuul
    [SerializeField]
    private AudioSource VoiceLine;

    // contains the door
    public GameObject Door;

    void Start()
    {
        // get all the scripts
        LetterL = FindObjectOfType<solvePuzzle>();
        LetterU = FindObjectOfType<solvePuzzle1>();
        LetterK = FindObjectOfType<solvePuzzle2>();
        LetterU2 = FindObjectOfType<solvePuzzle3>();
        LetterA = FindObjectOfType<solvePuzzle4>();

        // voice line is deactivated at the beginning
        VoiceLine.gameObject.SetActive(false);
    }

    void Update()
    {
        // if all letters are in right place, akuul will speak and the door will open
        if(LetterL.LetterLisActivated == true && LetterU.LetterUisActivated == true && LetterK.LetterKisActivated == true && LetterU2.LetterU2isActivated == true && LetterA.LetterAisActivated == true)
        {
            VoiceLine.gameObject.SetActive(true);
            Destroy(Door);
            
        }
    }
}
