﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this script is for the item interaction in the second level
public class Sequences : MonoBehaviour
{
    // the text for the press f button
    public GameObject pressF;

    // the audio source for a peace of the story
    public AudioSource Voice;

    // value, that shows if the letter is activated or not
    public bool LetterIsActivated;

    // contains the letter
    public GameObject Letter;

    // contains the particle system for the activation of the letter
    public GameObject Particlesystem;
    
    // contains the position where the stones should go up
    public GameObject MoveUp;

    // contains the position where the stones should go down
    public GameObject MoveDown;

    // the stones which should be moved
    public GameObject checkDistance;

    // shows if the player stays on the trigger or not
    private bool ItemIsActivated;
 
    void Start()
    {
        // the text of the f button is only visible when the player stays in the trigger
        pressF.SetActive(false);

        // it is activated when the player presses the f button
        Voice.gameObject.SetActive(false);        
        Letter.SetActive(false);
        LetterIsActivated = false;
        Particlesystem.SetActive(false);       
    }

    void Update()
    {
        // if the player leaves the trigger box the stones will move down
        if (ItemIsActivated == false)
        {
            checkDistance.transform.position = Vector3.MoveTowards(checkDistance.transform.position, MoveDown.transform.position, 2 * Time.deltaTime);
        }
    }

    // if the player walks into the trigger he can see that he is able to press the f key
    private void OnTriggerEnter(Collider other)
    {
        pressF.SetActive(true);
    }

    // if the player stays on the trigger he is allowed to press the f button
    // the stones will move up
    // a peace of the story will be told
    // the particle system and the letter will be activated
    private void OnTriggerStay(Collider other)        
    {
        ItemIsActivated = true;
        checkDistance.transform.position = Vector3.MoveTowards(checkDistance.transform.position, MoveUp.transform.position, 2 * Time.deltaTime);

        if (Input.GetKey(KeyCode.F))
        {               
                Voice.gameObject.SetActive(true);
                pressF.SetActive(false);
                Letter.SetActive(true);
                LetterIsActivated = true;               
                Particlesystem.SetActive(true);
        }                                                         
    }

    // if the player leaves the trigger box the voice will stop playing
    // the stones will move down
    // the player is not allowed to press the f key
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.name =="Player")
        {
            Voice.gameObject.SetActive(false);
            pressF.SetActive(false);
            ItemIsActivated = false;           
        }       
    }
  
}
