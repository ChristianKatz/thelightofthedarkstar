﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the respawn in the second level if the player falls off the map
public class Respawn : MonoBehaviour
{
    // position of the resapwn point
    private GameObject RespawnPoint;

    // value, that contains how far he is allowed to fall
    [SerializeField]
    private int RespawnValue;

    // player position for the respawn
    [SerializeField]
    private GameObject Player;

    // voice of the main character when he falls
    [SerializeField]
    private AudioSource JuukoFalls;
 
    // bool that will be true if the introdution talk between juuko and akuul is over
    private bool TalkIsOver;

    void Start()
    {
        // get the component
        RespawnPoint = GetComponent<GameObject>();

        // the audio shoudn't play at the beginning
        JuukoFalls.Stop();

        // is set to false at the beginning
        TalkIsOver = false;

        // the talk starts immedialtely if the player spawns in the second level
        StartCoroutine(JuukoFallsTimer());
    }

    void Update()
    {
        // if the player falls farther than the respawn value, he will respawn
        // if the talk is over, akuul explains why the player can't die, if he jumps down
        if (Player.transform.position.y < RespawnValue)
        {
            Player.transform.position = transform.position;
            

            if(TalkIsOver == true)
            {
                JuukoFalls.Play();
                StartCoroutine(SetOff());
            }
        }
    }
    // akuul will explain it to him when the introduction is over and if he falls again
    IEnumerator JuukoFallsTimer()
    {
        yield return new WaitForSeconds(45);
        TalkIsOver = true;       
    }

    // the explanation is just one time
    IEnumerator SetOff()
    {
        yield return new WaitForSeconds(1);
        TalkIsOver = false;
    }
}
