﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this script is for the fall when the player walks over the bridge to follow akuul
public class PlayerFallsDown : MonoBehaviour
{
    // contains the stoneway, which will be destroyed
    public GameObject Stoneway;

    // the voice of the character when the player falls
    public AudioSource JuukoFalls;

    // need this script to set the speed to 0
    private PlayerMovement playerMovement;

    // it deactivates the respawn
    public GameObject SetRespawnOff;

    private void Start()
    {
        // get the script
        playerMovement = FindObjectOfType<PlayerMovement>();

    }

    // if the player stepps into the trigger, the stonway will be destroyed and then the character begins to scream and is not able to move
    // it deactivates the respawn, because the player has to go to the next level and shouldn't respawn on the same level again
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            JuukoFalls.gameObject.SetActive(true);
            playerMovement.speed = 0;
            SetRespawnOff.SetActive(false);
            Destroy(Stoneway);
        }
    }
}
