﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the cursor will change when the menu shows up
public class CursorForMenu : MonoBehaviour
{
    // 2D texture for the cursor
    [SerializeField]
    private Texture2D CursorTexture;

    void Update()
    {
        //the cursor changes here
        Cursor.SetCursor(CursorTexture, new Vector2(0, 0), CursorMode.Auto);
    }
}
