﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// stores all letters that the player collects
public class LetterInventar : MonoBehaviour
{
    // these GameObjects contain the prefab of the letter
    [SerializeField]
    private GameObject FirstLetterCounter;
    [SerializeField]
    private GameObject SecondLetterCounter;
    [SerializeField]
    private GameObject ThirdLetterCounter;
    [SerializeField]
    private GameObject FourthLetterCounter;
    [SerializeField]
    private GameObject FifthLetterCounter;
    [SerializeField]
    private GameObject SixthLetterCounter;
    [SerializeField]
    private GameObject SeventhLetterCounter;
    [SerializeField]
    private GameObject EighthLetterCounter;
    [SerializeField]
    private GameObject NinthLetterCounter;
    [SerializeField]
    private GameObject TenthLetterCounter;
    [SerializeField]
    private GameObject EleventhLetterCounter;
    [SerializeField]
    private GameObject TwelfthLetterCounter;

    // these GameObjects contain the buttons to display the text
    [SerializeField]
    private GameObject FirstLetterButton;
    [SerializeField]
    private GameObject SecondLetterButton;
    [SerializeField]
    private GameObject ThirdLetterButton;
    [SerializeField]
    private GameObject FourthLetterButton;
    [SerializeField]
    private GameObject FifthLetterButton;
    [SerializeField]
    private GameObject SixthLetterButton;
    [SerializeField]
    private GameObject SeventhLetterButton;
    [SerializeField]
    private GameObject EighthLetterButton;
    [SerializeField]
    private GameObject NinthLetterButton;
    [SerializeField]
    private GameObject TenthLetterButton;
    [SerializeField]
    private GameObject EleventhLetterButton;
    [SerializeField]
    private GameObject TwelfthLetterButton;

    // these GameObjects contain the texts of the letters
    [SerializeField]
    private GameObject FirstLetterText;
    [SerializeField]
    private GameObject SecondLetterText;
    [SerializeField]
    private GameObject ThirdLetterText;
    [SerializeField]
    private GameObject FourthLetterText;
    [SerializeField]
    private GameObject FifthLetterText;
    [SerializeField]
    private GameObject SixthLetterText;
    [SerializeField]
    private GameObject SeventhLetterText;
    [SerializeField]
    private GameObject EighthLetterText;
    [SerializeField]
    private GameObject NinthLetterText;
    [SerializeField]
    private GameObject TenthLetterText;
    [SerializeField]
    private GameObject EleventhLetterText;
    [SerializeField]
    private GameObject TwelfthLetterText;

    // bool values which activate the letters in the inventory
    private bool FirstLetterIsActivated;
    private bool SecondLetterIsActivated;
    private bool ThirdLetterIsActivated;
    private bool FourthLetterIsActivated;
    private bool FifthLetterIsActivated;
    private bool SixthLetterIsActivated;
    private bool SeventhLetterIsActivated;
    private bool EighthLetterIsActivated;
    private bool NinthLetterIsActivated;
    private bool TenthLetterIsActivated;
    private bool EleventhLetterIsActivated;
    private bool TwelfthLetterIsActivated;

    // GameObject that contains the whole inventory to deactivate and activate it
    [SerializeField]
    private GameObject Inventar;

    // GameObject for the back button that goes back to the letter selection 
    [SerializeField]
    private GameObject BackButton;

    // bool value, that deactivates all letter buttons
    private bool activatedLetter;

    // bool value, that decides when the player is able to reopen the inventory
    private bool InventarIsActivated;

    // bool value, that deactivates the letter button
    private bool ButtonIsActivated = true;

    // bool value, that shows if the inventory is activated or deactivated
    public bool LetterInventoryisActivated;

    // need the script to deactivate the pause menu when the letter inventory has been opened
    private PauseMenu Pause;
 
    void Start()
    {
        // at the beginning all letter buttons are deactivated
        FirstLetterButton.SetActive(false);
        SecondLetterButton.SetActive(false);
        ThirdLetterButton.SetActive(false);
        FourthLetterButton.SetActive(false);
        FifthLetterButton.SetActive(false);
        SixthLetterButton.SetActive(false);
        SeventhLetterButton.SetActive(false);
        EighthLetterButton.SetActive(false);
        NinthLetterButton.SetActive(false);
        TenthLetterButton.SetActive(false);
        EleventhLetterButton.SetActive(false);
        TwelfthLetterButton.SetActive(false);
        BackButton.SetActive(false);
        Inventar.SetActive(false);
        activatedLetter = false;
        LetterInventoryisActivated = false;

        // get the script
        Pause = FindObjectOfType<PauseMenu>();
    }

    void Update()
    {
        // buttons of the letters will be deactivated if the player selects a letter he wants to read
        if(activatedLetter == true)
        {
            FirstLetterButton.SetActive(false);
            SecondLetterButton.SetActive(false);
            ThirdLetterButton.SetActive(false);
            FourthLetterButton.SetActive(false);
            FifthLetterButton.SetActive(false);
            SixthLetterButton.SetActive(false);
            SeventhLetterButton.SetActive(false);
            EighthLetterButton.SetActive(false);
            NinthLetterButton.SetActive(false);
            TenthLetterButton.SetActive(false);
            EleventhLetterButton.SetActive(false);
            TwelfthLetterButton.SetActive(false);
            ButtonIsActivated = false;
        }

        // if the pause menu has not been opened, the player can open the inventory with the E key
        // cursor will be visible
        // coroutine will be activated, that decides when the menu can be closed again
        if (Pause.PauseMenuIsActivated == false)
        {
            if (Input.GetKey(KeyCode.E))
            {
                Inventar.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                StartCoroutine(InventarActivated());
                LetterInventoryisActivated = true;
            }
        }
        
        // close the menu with the e key
        // cursor will be locked again
        // coroutine will start to decide when the player is able to reopen the menu
        if (Input.GetKey(KeyCode.E) && InventarIsActivated == true)
        {
            Inventar.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            StartCoroutine(InventarDeactivated());
            LetterInventoryisActivated = false;

            FirstLetterText.SetActive(false);
            SecondLetterText.SetActive(false);
            ThirdLetterText.SetActive(false);
            FourthLetterText.SetActive(false);
            FifthLetterText.SetActive(false);
            SixthLetterText.SetActive(false);
            SeventhLetterText.SetActive(false);
            EighthLetterText.SetActive(false);
            NinthLetterText.SetActive(false);
            TenthLetterText.SetActive(false);
            EleventhLetterText.SetActive(false);
            TwelfthLetterText.SetActive(false);
        }

        // it shows the button, that the player has already collected
        // "ButtonIsActivated" disables the many if condition to prevent the player from seeing the buttons to read the text clearly
        if (ButtonIsActivated == true)
        {
            if (FirstLetterCounter.activeInHierarchy == false)
            {
                FirstLetterButton.SetActive(true);
            }
            if (SecondLetterCounter.activeInHierarchy == false)
            {
                SecondLetterButton.SetActive(true);
            }
            if (ThirdLetterCounter.activeInHierarchy == false)
            {
                ThirdLetterButton.SetActive(true);
            }
            if (FourthLetterCounter.activeInHierarchy == false)
            {
                FourthLetterButton.SetActive(true);
            }
            if (FifthLetterCounter.activeInHierarchy == false)
            {
                FifthLetterButton.SetActive(true);
            }
            if (SixthLetterCounter.activeInHierarchy == false)
            {
                SixthLetterButton.SetActive(true);
            }
            if (SeventhLetterCounter.activeInHierarchy == false)
            {
                SeventhLetterButton.SetActive(true);
            }
            if (EighthLetterCounter.activeInHierarchy == false)
            {
                EighthLetterButton.SetActive(true);
            }
            if (NinthLetterCounter.activeInHierarchy == false)
            {
                NinthLetterButton.SetActive(true);
            }
            if (TenthLetterCounter.activeInHierarchy == false)
            {
                TenthLetterButton.SetActive(true);
            }
            if (EleventhLetterCounter.activeInHierarchy == false)
            {
                EleventhLetterButton.SetActive(true);
            }
            if (TwelfthLetterCounter.activeInHierarchy == false)
            {
                TwelfthLetterButton.SetActive(true);
            }
        }
    }

    // coroutine that decides when to reopen the inventory
    IEnumerator InventarActivated()
    {
        yield return new WaitForSeconds(0.2f);
        InventarIsActivated = true;
    }

    // coroutine that decides when to close the inventory
    IEnumerator InventarDeactivated()
    {
        yield return new WaitForSeconds(0.2f);
        InventarIsActivated = false;
    }

    // method for a button, that deactivates all letter texts and reactivates all buttons of the already collected letters
    public void Back()
    {
        FirstLetterText.SetActive(false);
        SecondLetterText.SetActive(false);
        ThirdLetterText.SetActive(false);
        FourthLetterText.SetActive(false);
        FifthLetterText.SetActive(false);
        SixthLetterText.SetActive(false);
        SeventhLetterText.SetActive(false);
        EighthLetterText.SetActive(false);
        NinthLetterText.SetActive(false);
        TenthLetterText.SetActive(false);
        EleventhLetterText.SetActive(false);
        TwelfthLetterText.SetActive(false);
        ButtonIsActivated = true;

        if(FirstLetterIsActivated == true)
        {
            FirstLetterButton.SetActive(true);
        }
        if (SecondLetterIsActivated == true)
        {
            SecondLetterButton.SetActive(true);
        }
        if (ThirdLetterIsActivated == true)
        {
            ThirdLetterButton.SetActive(true);
        }
        if(FourthLetterIsActivated == true)
        {
            FourthLetterButton.SetActive(true);
        }
        if (FifthLetterIsActivated == true)
        {
            FifthLetterButton.SetActive(true);
        }
        if (SixthLetterIsActivated == true)
        {
            SixthLetterButton.SetActive(true);
        }
        if (SeventhLetterIsActivated == true)
        {
            SeventhLetterButton.SetActive(true);
        }
        if (EighthLetterIsActivated == true)
        {
            EighthLetterButton.SetActive(true);
        }
        if (NinthLetterIsActivated == true)
        {
            NinthLetterButton.SetActive(true);
        }
        if (TenthLetterIsActivated == true)
        {
            TenthLetterButton.SetActive(true);
        }
        if (EleventhLetterIsActivated == true)
        {
            EleventhLetterButton.SetActive(true);
        }
        if (TwelfthLetterIsActivated == true)
        {
            TwelfthLetterButton.SetActive(true);
        }
        BackButton.SetActive(false);
        activatedLetter = false;
    }

    // many button methods for the different letters
    // if you click on a button the text of the selected letter will appear
    // the back button will appear as well to return to the letter selection
    public void FirstLetter()
    {
        FirstLetterIsActivated = true;
        FirstLetterText.SetActive(true);
        activatedLetter = true;     
        BackButton.SetActive(true);
    }
    public void SecondLetter()
    {
        SecondLetterIsActivated = true;
        SecondLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }
    public void ThirdLetter()
    {
        ThirdLetterIsActivated = true;
        ThirdLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }

    public void FourthLetter()
    {
        FourthLetterIsActivated = true;
        FourthLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);

    }

    public void FifthLetter()
    {
        FifthLetterIsActivated = true;
        FifthLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }
    public void SixthLetter()
    {
        SixthLetterIsActivated = true;
        SixthLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }
    public void SeventhLetter()
    {
        SeventhLetterIsActivated = true;
        SeventhLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }
    public void EightLetter()
    {
        EighthLetterIsActivated = true;
        EighthLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }
    public void NinthLetter()
    {
        NinthLetterIsActivated = true;
        NinthLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }

    public void TenthLetter()
    {
        TenthLetterIsActivated = true;
        TenthLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }

    public void EleventhLetter()
    {
        EleventhLetterIsActivated = true;
        EleventhLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }

    public void TwelfthLetter()
    {
        TwelfthLetterIsActivated = true;
        TwelfthLetterText.SetActive(true);
        activatedLetter = true;
        BackButton.SetActive(true);
    }







}
