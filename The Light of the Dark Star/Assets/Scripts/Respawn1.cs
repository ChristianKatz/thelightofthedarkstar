﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// respawn for the player in the first level
public class Respawn1 : MonoBehaviour
{
    // the respawn position
    private GameObject RespawnPoint;

    // value, that contains how far he is allowed to fall
    [SerializeField]
    private int RespawnValue;

    // player position for the respawn
    [SerializeField]
    private GameObject Player;

    // voice of the main character when he falls
    [SerializeField]
    private AudioSource JuukoFalls;

    // value, that determines when the voice starts
    [SerializeField]
    private int JuukoPosition;

    void Start()
    {
        // get the component
        RespawnPoint = GetComponent<GameObject>();

        // the voice is deactivated at the beginning
        JuukoFalls.gameObject.SetActive(false);            
    }

    void Update()
    {
        // if the player falls farther than the respawn value he will respawn
        if (Player.transform.position.y < RespawnValue)
        {
            Player.transform.position = transform.position;
            JuukoFalls.gameObject.SetActive(false);
                              
        }
        // the audio starts to play at a certain value when he falls
        if(Player.transform.position.y < JuukoPosition)
        {
            JuukoFalls.gameObject.SetActive(true);
        }
    }
}
