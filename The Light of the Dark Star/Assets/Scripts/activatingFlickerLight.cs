﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activatingFlickerLight : MonoBehaviour
{
    // the script where the flicker light will be activated
    private FlickerLight flicker;

    // the script decides if the flickerlight should be activated
    private getMedallion GetMedallion;

    void Start()
    {
        // get the two scripts
        flicker = GetComponent<FlickerLight>();
        GetMedallion = FindObjectOfType<getMedallion>();
    }

    void Update()
    {
        // if the player got the medaillon, the flicker light will be activated
        if(GetMedallion.activateLight == true)
        {
            flicker.enabled = true;
        }
    }
}
