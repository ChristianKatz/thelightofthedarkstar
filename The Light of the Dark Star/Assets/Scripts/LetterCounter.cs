﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// this script counts the letter in the first level
public class LetterCounter : MonoBehaviour
{
    // it displays the number of letters
    private TextMeshProUGUI Letter;

    // GameObjects, which lead to the next level
    [SerializeField]
    private  GameObject WayToTheNextLevel;

    // the counter for the letter
    private int counting;  
    public int Counting
    { 
    get

      {
        return counting;
      }

    set

      {
        counting = value;
      }
    }
  
    void Start()
    {
        // get the component
        Letter = GetComponent<TextMeshProUGUI>();

        // the Way to the next level is deactivated at the beginning
        WayToTheNextLevel.SetActive(false);
    }
  
    void Update()
    {
        // it displays the collected number of the letters
        Letter.text = string.Format("Letter {0,0} /12", counting);

        // if the player finds all 12 letters the way to the next level appears
        if (Counting == 12)
        {
            WayToTheNextLevel.SetActive(true);
        }
    }
}
