﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the script is attached on the container for the letters
public class solvePuzzle3 : MonoBehaviour
{
    // shows if the correct letter is in the container
    public bool LetterU2isActivated;

    // that represents the letter color
    [SerializeField]
    private GameObject activateParticlesystem;

    void Start()
    {
        // at the beginning is nothing in the container
        LetterU2isActivated = false;

        // the particle system is deactivated at the beginning
        activateParticlesystem.SetActive(false);
    }

    // if the second letter U stays on the correct place, the bool value will set to true
    // the particle system will start 
    public void OnTriggerStay(Collider other)
    {      
        if(other.gameObject.name == "SecondLetterU")
        {
            LetterU2isActivated = true;
            activateParticlesystem.SetActive(true);
        }                       
    }

    // if the second letter U isn't in the container, the bool value will set to false
    // the particle system will be deactivated if the letter is not in the correct container
    public void OnTriggerExit(Collider other)
    {     
        if (other.gameObject.name == "SecondLetterU")
        {
            LetterU2isActivated = false;
            activateParticlesystem.SetActive(false);
        }       
    }
}
