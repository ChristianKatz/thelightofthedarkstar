﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 // the script is attached on the container for the letters
public class solvePuzzle : MonoBehaviour
{
    // shows if the correct letter is in the container
    public bool LetterLisActivated;
    
    void Start()
    {
        // at the beginning is nothing in the container
        LetterLisActivated = false;       
    }

    // if the letter L stays on the correct place, the bool value will set to true
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "LetterL")
        {
            LetterLisActivated = true;
        }
    }

    // if the letter L isn't in the correct container, the bool value will set to false
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "LetterL")
        {
                LetterLisActivated = false;
        }
    }


}
