﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the movement for the letters to get them in the right spot to open the door in the second level
public class Puzzle : MonoBehaviour
{
    // texture for the pointer
    [SerializeField]
    private Texture2D CursorTexture;

    // need this script to get the default cursor texture 
    private PlayerMovement playerMovement;

    // the letter that should be moved
    public GameObject Letter;

    // the respawn place of the letters
    public GameObject Respawn;

    // the distance of the letters on the z-axis if the player picks one with the cursor
    public float distance;

    // contains the value how far the player is away of the respawn position
    private float gapY;
    private float gapX;
    private float gapZ;
   
    // if the player clicks on a letter, the object should move with the cursor
    private void OnMouseDrag()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        Letter.transform.position = objPosition;
    }

    //change the texture of the cursor if he points on a letter
    private void OnMouseEnter()
    {
        Cursor.SetCursor(CursorTexture, new Vector2(0, 0), CursorMode.Auto);      
    }

    // change the cursor back
    private void OnMouseExit()
    {
        Cursor.SetCursor(playerMovement.CursorTexture, new Vector2(0, 0), CursorMode.Auto);        
    }

    private void Start()
    {
        // get the script
        playerMovement = FindObjectOfType<PlayerMovement>();
    }

    private void Update()
    {
        // gap calculation betwenn the respawn position and letter position
        gapY = Respawn.transform.position.y - Letter.transform.position.y;
        gapX = Respawn.transform.position.x - Letter.transform.position.x;
        gapZ = Respawn.transform.position.z - Letter.transform.position.z;

        // if the gap is bigger than the numbers, the letter will respawn 
        if (gapY > 5 )
        {
            Letter.transform.position = Respawn.transform.position;
        }
        if (gapX > 2|| gapX < -5)
        {
            Letter.transform.position = Respawn.transform.position;
        }
        if(gapZ > 10 || gapZ < -10)
        {
            Letter.transform.position = Respawn.transform.position;
        }
    }
    





}
