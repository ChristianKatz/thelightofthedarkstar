﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// that is the letter interaction in the first level
public class ItemInteraction : MonoBehaviour
{
    // the particle system will be deactivated when the player interacts with the item
    [SerializeField]
    private GameObject PartikclSystem;

    // button f text for the player
    public GameObject PressF;

    // text of the Letter
    public GameObject Text;

    // The letter prefab to activated or deactivated it
    [SerializeField]
    private GameObject Letter;

    // need this script to change the cursor of the player
    private PlayerMovement playerMovement;

    // Cursor texture for the change
    [SerializeField]
    private Texture2D CursorTexture;

    // the bool value, that allowes the player to press the f button again to take the letter in the inventory
    private bool deactivateText;

    // activates the letter in the inventory
    private bool activateLetter;
    public bool ActivateLetter
    {
        get
        {
            return activateLetter;
        }
        set
        {
            activateLetter = value;
        }
    }
 

    void Start()
    {
        // get the script
        playerMovement = FindObjectOfType<PlayerMovement>();

        // the player should only see the f button when he is in the trigger box
        PressF.SetActive(false);

        // the text is only active, when the player presses the f button in the trigger box
        Text.SetActive(false);
        
        // the letter is only available in the inventory when the player reads the letter
        activateLetter = false;

        // after the player pressed the f button it will be set to true to press the f button again
        deactivateText = false;                     
    }

    // coroutine to deactivate the letter after the player pressed the f button
    IEnumerator DeactivatingText()
    {
        yield return new WaitForSeconds(0.2f);
        deactivateText = true;
    }

    // when the player stepps in the trigger box, he is able to see that he can press the f button
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            PressF.SetActive(true);
        }
    }

    // if the player stays in the trigger box and press the f button, he can read the letter
    // cursor is deactivated because otherwise it would disturb the reading
    // particle system is deactivated
    // coroutine starts to allow the player to press the f button again
    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKey(KeyCode.F))
        {
            Text.SetActive(true);
            PressF.SetActive(false);
            activateLetter = true;
            Cursor.visible = false;
            StartCoroutine(DeactivatingText());
            PartikclSystem.SetActive(false);                       
        }

        // if the player presses the f button again, the text will disappear, but it will appear at the inventory
        // cursor is visible again
        // the text will disappear
        // letter will disappear
        if (Input.GetKey(KeyCode.F) && deactivateText == true)
        {          
            Text.SetActive(false);
            Cursor.visible = true;
            Letter.SetActive(false);
        }
    }

    // if the player reads the letter and goes out the trigger box, the text will be deactivated
    // the cursor is visible again
    // if the player reads the letter, didn't press the f button again and walked out of the trigger box, the letter will be in the inventory
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            PressF.SetActive(false);
            Text.SetActive(false);
            Cursor.visible = true;

            if(activateLetter == true)
            {
                Letter.SetActive(false);
            }
        }
    }

    // if the player points on the letter the new cursor texture will appear
    private void OnMouseEnter()
    {
        Cursor.SetCursor(CursorTexture, new Vector2(0, 0), CursorMode.Auto);
    }
    // if the player leaves the letter with the cursor the old texture will appear again
    private void OnMouseExit()
    {
        Cursor.SetCursor(playerMovement.CursorTexture, new Vector2(0, 0), CursorMode.Auto);
    }
}
  
