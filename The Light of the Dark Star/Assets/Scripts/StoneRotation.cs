﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// that is the script for the debris
public class StoneRotation : MonoBehaviour
{
    // contains the debris
    public GameObject[] Stones;

    // the rotation speed of the debris
    public float RotationSpeed;


    // continuously rotates the debris 
    void Update()
    {
        Stones[0].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[0].transform.RotateAround(new Vector3(0,0,0), new Vector3(0, 1, 0), 0);

        Stones[1].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[1].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[2].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[2].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[3].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[3].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[4].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[4].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[5].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[5].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[6].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[6].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[7].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[7].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[8].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[8].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[9].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[9].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[10].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[10].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[11].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[11].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[12].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[12].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[13].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[13].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[14].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[14].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[15].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[15].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[16].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[16].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[17].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[17].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[18].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[18].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[19].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[19].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[20].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[20].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);

        Stones[21].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[21].transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), 0);             
    }
}
