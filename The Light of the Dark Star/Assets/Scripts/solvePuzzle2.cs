﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// the script is attached on the container for the letters
public class solvePuzzle2 : MonoBehaviour
{
    // shows if the correct letter is in the container
    public bool LetterKisActivated;

    // that represents the letter color
    [SerializeField]
    private GameObject activateParticlesystem;

    void Start()
    {
        // at the beginning is nothing in the container
        LetterKisActivated = false;

        // the particle system is deactivated at the beginning
        activateParticlesystem.SetActive(false);
    }

    // if the letter K stays on the correct place, the bool value will set to true
    // the particle system will start 
    public void OnTriggerStay(Collider other)
    {      
        if(other.gameObject.name == "LetterK")
        {
            LetterKisActivated = true;
            activateParticlesystem.SetActive(true);
        }                       
    }

    // if the letter K isn't in the container, the bool value will set to false
    // the particle system will be deactivated if the letter is not in the correct container
    public void OnTriggerExit(Collider other)
    {     
        if (other.gameObject.name == "LetterK")
        {
            LetterKisActivated = false;
            activateParticlesystem.SetActive(false);
        }       
    }
}
