﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// camera movement for the character
public class CameraMovement : MonoBehaviour
{
    // float value, how fast the camera should be moved
    [SerializeField]
    private float speed;      

    // float value to clamp the camera angle
    private float yAxisClamp;

    // Transform to have the camera position
    private Transform CameraTransform;
               
    void Start()
    {       
        // get the camera transform
        CameraTransform = GetComponent<Transform>();                
    }

    void Update()
    {
        // get the float value, where the mouse is moved to a certain speed
        float moveVertical = Input.GetAxis("Mouse Y") * speed * Time.deltaTime;
        
        // get the value in which position the mouse is in the moment
        yAxisClamp += moveVertical;

        // clamp the angle 
        yAxisClamp = Mathf.Clamp(yAxisClamp, -45f, 45f);

        // the camera can be moved between these certain angles
        Quaternion CameraRotation = Quaternion.Euler(yAxisClamp, 0f, 0f);

        // the angle will be transmitted to the camera
        CameraTransform.localRotation = CameraRotation;
    }   
}
