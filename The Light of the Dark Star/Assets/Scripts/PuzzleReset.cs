﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this script activates the respawn for the puzzle
public class PuzzleReset : MonoBehaviour
{
    // need this script to know if the player interactes with the item
    private Sequences sequences;

    // GameObject that contains the reset area
    [SerializeField]
    private GameObject Reset;

    void Start()
    {
        // get the sequence script
        sequences = FindObjectOfType<Sequences>();       

        // the reset area shouldn't be activated at the beginning
        Reset.SetActive(false);       
    }

    void Update()
    {
        // if the player interacts with a sequence, the reset area will be activated
        if (sequences.LetterIsActivated == true)
        {
            Reset.SetActive(true);            
        }
    }    
}
