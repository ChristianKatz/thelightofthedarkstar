﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Decision between lukua and akuul at the final
// this script is attached on lukua as well as akuul
public class Decision : MonoBehaviour
{
    // text that the player should give the medaillon
    [SerializeField]
    private GameObject Text;

    // deactivate the decision of the other one
    [SerializeField]
    private GameObject deactivateDecision;
    [SerializeField]

    // the final voice of lukua or akuul
    private AudioSource LukuaOrAkuul;

    //the case, when the player should decide before the talk between lukua and akuul is over
    [SerializeField]
    private AudioSource StopConversation;

    // bool value that shows if the f key can be pressed or not
    private bool pressF = true;

    void Start()
    {
        // deactivate the text in the beginning
        Text.SetActive(false);
        
        // as long as no decision is fallen both texts are visible
        deactivateDecision.SetActive(true);
        LukuaOrAkuul.Stop();              
    }

    // if the player walks into the trigger the text will appear, that he should give the medaillon to him
    private void OnTriggerEnter(Collider other)
    {
        Text.SetActive(true);
    }

    // the player stays on the trigger and chooses one of the two characters
    // the text will be deactivated and the voice will be activated
    // the other decsision from the character will be deactivated
    // coroutine will be started
    // conversation between lukua and akuul will be stopped if the talk is not over already
    // f button will be deactivated
    private void OnTriggerStay(Collider other)
    {
        if (pressF == true)
        {
            if (Input.GetKey(KeyCode.F))
            {               
                Text.SetActive(false);
                LukuaOrAkuul.Play();
                deactivateDecision.SetActive(false);
                StartCoroutine(DecisionMaking());
                StopConversation.Stop();
                pressF = false;
            }
        }
    }

    // should the player leaves the trigger the text will be deactivated
    private void OnTriggerExit(Collider other)
    {
        Text.SetActive(false);
    }

    // Coroutine that leads back to the menu
    // Cursor is moveable for the menu
    IEnumerator DecisionMaking()
    {
        yield return new WaitForSeconds(22f);
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("Menu");         
    }
}
