﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// the pause menu in the game
public class PauseMenu : MonoBehaviour
{
    // GameObject, that contains the whole pause menu
    public GameObject Pause;

    // contains the scene name
    public string SceneName;

    // need this script to know if the letter inventory is activated
    private LetterInventar LetterInventory;

    // it shows if the pause menu is open or not
    public bool PauseMenuIsActivated;

    void Start()
    {
        // get the scripts
        LetterInventory = FindObjectOfType<LetterInventar>();

        // deactivate the pause menu at the beginning because only the player is allowed to open and close it
        Pause.SetActive(false);

        // the cursor is locked when the game starts
        Cursor.lockState = CursorLockMode.Locked;

        // the menu is deactivated at the beginning
        PauseMenuIsActivated = false;
    }

    void Update()
    {
        // when the inventory is not open, the player is allowed to open the pause menu
        // the pasue menu will be activated
        // the cursor is moveable
        if (LetterInventory.LetterInventoryisActivated == false)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Pause.SetActive(true);
                PauseMenuIsActivated = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }

    // button for a method, that leads back to the game
    // the cursor is locked again
    // the player is able to move again
   public void Resume()
    {
        Pause.SetActive(false);
        PauseMenuIsActivated = false;
        Cursor.lockState = CursorLockMode.Locked;
     
    }

    // method for a button, that leads back to the menu
    public void BackToTheMainMenu()
    {
        SceneManager.LoadScene(SceneName);
        PauseMenuIsActivated = false;
    }
}
